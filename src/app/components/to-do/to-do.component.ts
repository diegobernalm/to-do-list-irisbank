import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { ToDoList } from '../../interfaces/to-do-list'

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss']
})
export class ToDoComponent implements OnInit {
  // @ts-ignore
  toDoListForm: FormGroup;
  toDoList: ToDoList[] = [
    { name: 'All', tasks: [] },
    { name: 'Monday', tasks: [] },
  ]
  currentSelection = 'All';
  currentTasks: string[] = [];


  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.toDoListForm = this.fb.group({
      selection: new FormControl('All'),
      task: new FormControl()
    });
  }

  submit() {
    this.toDoList.map((item) => {
      if (item.name === this.toDoListForm.value.selection)
        item.tasks.push(this.toDoListForm.value.task)
    });
    this.updateSelection()
    this.toDoListForm.setValue({ task: '', selection: this.toDoListForm.value.selection })
  }

  updateSelection() {
    this.currentSelection = this.toDoListForm.value.selection;
    const item = this.toDoList.find(element => element.name === this.currentSelection) || { tasks: [] };
    this.currentTasks = item.tasks
  }

  deleteItem(task: string) {
    this.toDoList.map((item) => {
      if (item.name === this.toDoListForm.value.selection)
        item.tasks.splice(item.tasks.indexOf(task), 1);
    });
  }
}
