# Prueba de conocimiento - Frontend

Proyecto donde se implementa una ventana de To Do List funcional donde se incluyen y eliminan tareas.

## Instalación y debug

En el directorio raiz del proyecto ejecuta el siguiente comando:

```sh
npm i
ng serve
```